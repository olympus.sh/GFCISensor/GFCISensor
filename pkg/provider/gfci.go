package provider

import (
	"github.com/google/uuid"
	"net"
	"os"
)

// GFCISensor The Sensor's Structure data that the client will leverage to identify itself.
type GFCISensor struct {
	Id            string `json:"id"`
	PrivateIpAddr string `json:"private_ip_addr"`
	Hostname      string `json:"hostname"`
}

// TODO: We may want a username/password at some point.
// The client will need to write something the id to the filesystem in the pi.
// if no ID it will call GenerateId()

// GenerateId will return a unique UUID for the Sensor ID.
func (g *GFCISensor) GenerateId() string {
	return uuid.New().String()
}

func (g *GFCISensor) GetHostname() (string, error) {
	return os.Hostname()
}

func (g *GFCISensor) GetIPAddress() (string, error) {
	var result net.IPAddr
	if addrs, ok := net.InterfaceAddrs(); ok != nil {
		return "", ok
	} else {
		for _, address := range addrs {
			// check the address type and if it is not a loopback the display it
			if ipnet, ok := address.(*net.IPAddr); ok && !ipnet.IP.IsLoopback() {
				if ipnet.IP != nil {
					result = *ipnet
				}
			}
		}
		return result.String(), nil
	}
}
