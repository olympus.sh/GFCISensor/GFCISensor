package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/olympus.sh/GFCISensor/GFCISensor/pkg/provider"
	"io/ioutil"
	"os"
	"path/filepath"
)

const (
	sensorInitDirpath   = ".olympus"
	sensorInitDirMode   = 0755
	sensorStateFilename = ".gfci-sensor"
)

func main() {
	home, err := os.UserHomeDir()
	//var sensorState *os.File
	var sensorState string
	if err == nil {
	}

	sensorDirPath := home + string(filepath.Separator) + sensorInitDirpath
	sensorFilepath := sensorDirPath + string(filepath.Separator) + sensorStateFilename

	if _, err := os.Stat(sensorDirPath); os.IsNotExist(err) {
		ferr := os.MkdirAll(sensorDirPath, sensorInitDirMode)
		if ferr != nil {
			panic(ferr)
		}
	}

	sensor := provider.GFCISensor{}

	if _, err := os.Stat(sensorFilepath); os.IsNotExist(err) {
		// create and write out the sensor data.. Unmarshall
		//sensorState, ferr = os.OpenFile(sensorFilepath, os.O_CREATE, 0644)
		if h, ok := sensor.GetHostname(); ok == nil {
			sensor.Hostname = h
		}
		if i, ok := sensor.GetIPAddress(); ok == nil {
			sensor.PrivateIpAddr = i
		}

		fmt.Println("Creating SensorState File")
	} else {
		content, ferr := ioutil.ReadFile(sensorFilepath)
		sensorState = string(content)
		if ferr != nil {
			//_ = sensorState.Close()
			panic(ferr)
		}
		fmt.Println("Opening Readonly SensorState File")
	}

	fmt.Println(sensorDirPath)
	fmt.Println(sensorFilepath)
	fmt.Println(sensorState)
	fmt.Println(json.Marshal(sensor))

}

//file, err := os.Open()
//if err != nil {
//}
//defer file.close()
//
//return bufio.NewReader(file)
//sensor := provider.GFCISensor{}
